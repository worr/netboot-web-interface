package Netboot::Schema::Result::Images;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("images");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('images_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "name",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 20,
  },
  "type_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "options",
  {
    data_type => "character varying[]",
    default_value => undef,
    is_nullable => 1,
    size => 40,
  },
  "user_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "pxelinux_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "pxefreebsd_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "pxegrub_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("images_name_key", ["name"]);
__PACKAGE__->add_unique_constraint("images_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "pxegrub_id",
  "Netboot::Schema::Result::Pxegrub",
  { id => "pxegrub_id" },
);
__PACKAGE__->belongs_to(
  "user_id",
  "Netboot::Schema::Result::Users",
  { id => "user_id" },
);
__PACKAGE__->belongs_to(
  "type_id",
  "Netboot::Schema::Result::Types",
  { id => "type_id" },
);
__PACKAGE__->belongs_to(
  "pxefreebsd_id",
  "Netboot::Schema::Result::Pxefreebsd",
  { id => "pxefreebsd_id" },
);
__PACKAGE__->belongs_to(
  "pxelinux_id",
  "Netboot::Schema::Result::Pxelinux",
  { id => "pxelinux_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:st44tAKwd84J3c+YVzBfyQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
