package Netboot::Controller::List;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Netboot::Controller::List - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(images => [$c->model('DB::Images')->image_information]);
    $c->stash(template => 'list/all.mas');
}

sub pxelinux :Local {
    my ( $self, $c ) = @_;

    $c->stash(images => [$c->model('DB::Images')->image_information->pxelinux]);
    $c->stash(template => 'list/pxelinux.mas');
}

sub pxegrub :Local {
    my ($self, $c ) = @_;

    $c->stash(images => [$c->model('DB::Images')->image_information->pxegrub]);
    $c->stash(template => 'list/pxegrub.mas');
}

sub pxefreebsd :Local {
    my ( $self, $c ) = @_;

    $c->stash(images => [$c->model('DB::Images')->image_information->pxefreebsd]);
    $c->stash(template => 'list/pxefreebsd.mas');
}


=head1 AUTHOR

,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
