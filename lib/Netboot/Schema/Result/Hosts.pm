package Netboot::Schema::Result::Hosts;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("hosts");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('hosts_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "hostname",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 20,
  },
  "ipaddr",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "user_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "options",
  {
    data_type => "character varying[]",
    default_value => undef,
    is_nullable => 1,
    size => 60,
  },
);
__PACKAGE__->set_primary_key("id", "hostname");
__PACKAGE__->add_unique_constraint("hosts_pkey", ["id", "hostname"]);
__PACKAGE__->belongs_to(
  "user_id",
  "Netboot::Schema::Result::Users",
  { id => "user_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KpndqyhEI+gNrVJBZUp5Uw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
