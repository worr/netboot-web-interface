package Netboot::NetworkSchema::Result::PortChanges;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("port_changes");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('port_changes_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "switch",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "port",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "start_time",
  {
    data_type => "timestamp without time zone",
    default_value => "'2003-11-20 12:26:25.02486'::timestamp without time zone",
    is_nullable => 0,
    size => 8,
  },
  "end_time",
  {
    data_type => "timestamp without time zone",
    default_value => "'2003-11-20 12:26:25.02486'::timestamp without time zone",
    is_nullable => 0,
    size => 8,
  },
  "active",
  { data_type => "boolean", default_value => undef, is_nullable => 0, size => 1 },
  "change_user",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "reason",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("port_changes_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "port",
  "Netboot::NetworkSchema::Result::Ports",
  { port => "port", switch => "switch" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cUVsmsn7DjNT1uXRU6yjtg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
