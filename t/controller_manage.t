use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Netboot' }
BEGIN { use_ok 'Netboot::Controller::manage' }

ok( request('/manage')->is_success, 'Request should succeed' );
done_testing();
