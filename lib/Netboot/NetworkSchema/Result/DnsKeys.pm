package Netboot::NetworkSchema::Result::DnsKeys;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("dns_keys");
__PACKAGE__->add_columns(
  "key_name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "key",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("key_name");
__PACKAGE__->add_unique_constraint("dns_keys_pkey", ["key_name"]);
__PACKAGE__->has_many(
  "dns_zones",
  "Netboot::NetworkSchema::Result::DnsZones",
  { "foreign.key_name" => "self.key_name" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:W39zCibLkiNgaQnUBgwRmQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
