package Netboot::Controller::Upload;
use Moose;
use namespace::autoclean;

use Archive::Tar;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Netboot::Controller::upload - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(template => 'upload/upload.mas');
}

sub error :Private {
    my ( $self, $c, $error_message ) = @_;

    $c->error($error_message);
    die;
}

# Looks for % options in config files
sub parse_options :Private {
    my ( $self, $pxelinux ) = @_;
    my @options;
    my $option;

    open PXELINUX_CFG, $pxelinux;

    foreach (<PXELINUX_CFG>) {
        # Matches text bounded in % as many times as appears on a line
        while (/%(.+?)%/g) {
            $option = lc $1;
            $option =~ s/[^\d^\w]/_/g;
            push @options, $option if not (grep {$_ eq $option} @options);
        }
    }

    return @options;
}

# Untar files in uncompressed tar files
sub untar_files :Private {
    my ( $self, $target_directory, $suffix ) = @_;
    my $tar_file = "$target_directory/$suffix";
    my $tar = Archive::Tar->new($tar_file);

    chdir $target_directory or die "Cannot change directory";
    $tar->extract or die "Could not extract";
    unlink $tar_file;
}

# Check for non-unique names
# returns 1 if name is unique
sub check_name {
    my ( $self, $c, $name ) = @_;

    my @res = $c->model('DB::Images')->search({name => '$name'});

    return (@res == 0);
}

# Handles pxelinux uploads
sub upload_pxelinux :Local {
    my ( $self, $c ) = @_;
    my $upload_base = "/srv/images/pxelinux/";

    my $name = $c->request->params->{"name"};
    my $pxelinux_cfg = $c->request->upload("pxelinux_cfg");
    my $tftproot = $c->request->upload("tftproot");

    $self->check_name($c, $name) or $self->error($c, "Non-unique name");

    my $image_directory = $upload_base . $name;
    my $tftproot_directory  = "$image_directory/tftproot/";
    mkdir $image_directory;
    mkdir $tftproot_directory;

    $pxelinux_cfg->copy_to("$image_directory/pxelinux.cfg");
    $tftproot->copy_to("$tftproot_directory/tftproot.tar");

    $self->untar_files($tftproot_directory, "tftproot.tar");

    my @options = $self->parse_options("$image_directory/pxelinux.cfg");

    my $pxelinux = $c->model('DB::Pxelinux')->create({
                                            pxelinux_cfg => "$image_directory/pxelinux.cfg",
                                            tftproot => "$image_directory/tftproot/",
                                        });

    my $image = $c->model('DB::Images')->create({
                                            name => "$name",
                                            options => \@options,
                                            pxelinux_id => {
                                                id => $pxelinux->id,
                                            },
                                            user_id => {
                                                username => "worr",
                                            },
                                            type_id => {
                                                name => "pxelinux",
                                            },
                                        });

    $c->stash({ template => 'upload/completed.mas',
                success => 1 });
}

# Handles pxefreebsd uploads
sub upload_pxefreebsd :Local {
    my ( $self, $c ) = @_;
    my $upload_base = "/srv/images/pxefreebsd/";

    my $name = $c->request->params->{"name"};
    my $pxeboot = $c->request->upload("pxeboot");
    my $loader_rc = $c->request->upload("loader_rc");
    my $tftproot = $c->request->upload("tftproot");
    my $nfsroot = $c->request->upload("nfsroot");

    $self->check_name($c, $name) or $self->error($c, "Non-unique name");

    my $image_directory = $upload_base . $name;
    mkdir $image_directory;

    $pxeboot->copy_to("$image_directory/pxeboot");
    $loader_rc->copy_to("$image_directory/loader.rc");

    my $tftproot_directory  = "$image_directory/tftproot/";
    my $nfsroot_directory  = "$image_directory/nfsroot/";
    mkdir $tftproot_directory;
    mkdir $nfsroot_directory;

    $tftproot->copy_to("$tftproot_directory/tftproot.tar");
    $nfsroot->copy_to("$nfsroot_directory/nfsroot.tar");

    $self->untar_files($tftproot_directory, "tftproot.tar");
    $self->untar_files($nfsroot_directory, "nfsroot.tar");

    $self->parse_options("$image_directory/pxeboot");
    $self->parse_options("$image_directory/loader.rc");

    my $pxefreebsd = $c->model('DB::Pxefreebsd')->create({
                                            pxeboot => "$image_directory/pxeboot",
                                            loader_rc => "$image_directory/loader.rc",
                                            tftproot => "$image_directory/tftproot/",
                                            nfsroot => "$image_directory/nfsroot/",
                                        });

    my $image = $c->model('DB::Images')->create({
                                            name => "$name",
                                            pxefreebsd_id => {
                                                id => $pxefreebsd->id,
                                            },
                                            user_id => {
                                                username => "worr",
                                            },
                                            type_id => {
                                                name => "pxefreebsd",
                                            },
                                        });

    $c->stash({ template => 'upload/completed.mas',
                success => 1 });
}

# To be implemented
sub upload_pxegrub :Local { }

# To get custom error messages
sub end :Private {
    my ( $self, $c ) = @_;

    if (scalar @{$c->error}) {
        $c->stash->{errors} = $c->error;
        $c->stash->{template} = 'errors.mas';
        $c->forward('Netboot::View::Mason');
        $c->error(0);
    }

    return 1 if $c->response->status =~ /^3\d\d$/;
    return 1 if $c->response->body;

    unless ($c->response->content_type) {
        $c->response->content_type('text/html; charset=utf-8');
    }

    $c->forward('Netboot::View::Mason');
}

=head1 AUTHOR

,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
