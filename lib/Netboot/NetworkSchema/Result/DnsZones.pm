package Netboot::NetworkSchema::Result::DnsZones;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("dns_zones");
__PACKAGE__->add_columns(
  "zone",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "key_name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "nameserver",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "forward",
  { data_type => "boolean", default_value => undef, is_nullable => 0, size => 1 },
);
__PACKAGE__->set_primary_key("zone");
__PACKAGE__->add_unique_constraint("dns_zones_pkey", ["zone"]);
__PACKAGE__->has_many(
  "address_ranges",
  "Netboot::NetworkSchema::Result::AddressRanges",
  { "foreign.domain" => "self.zone" },
);
__PACKAGE__->belongs_to(
  "key_name",
  "Netboot::NetworkSchema::Result::DnsKeys",
  { key_name => "key_name" },
);
__PACKAGE__->has_many(
  "hosts",
  "Netboot::NetworkSchema::Result::Hosts",
  { "foreign.domain" => "self.zone" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6WJz94y+bHziSyeHkBEdvg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
