package Netboot::Schema::ResultSet::Users;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub isadmin {
    my ( $self, $username ) = @_;

    return $self->search({ username => $username }, { select => 'admin' });
}

sub get_id {
    my ( $self, $username ) = @_;

    return $self->search({ username => $username}, { select => 'id' });
}

1;
