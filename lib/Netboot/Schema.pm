package Netboot::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cYvOHVZmc1hiBjbOAcUPNw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
