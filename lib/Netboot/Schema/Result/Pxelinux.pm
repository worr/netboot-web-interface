package Netboot::Schema::Result::Pxelinux;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("pxelinux");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('pxelinux_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "pxelinux_cfg",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "tftproot",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("pxelinux_pxelinux_cfg_key", ["pxelinux_cfg"]);
__PACKAGE__->add_unique_constraint("pxelinux_pkey", ["id"]);
__PACKAGE__->add_unique_constraint("pxelinux_tftproot_key", ["tftproot"]);
__PACKAGE__->has_many(
  "images",
  "Netboot::Schema::Result::Images",
  { "foreign.pxelinux_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gGtNRwTJOB2dAPYrfkI6Yg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
