package Netboot::NetworkSchema::Result::Networks;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("networks");
__PACKAGE__->add_columns(
  "network",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("network");
__PACKAGE__->add_unique_constraint("networks_pkey", ["network"]);
__PACKAGE__->has_many(
  "ip6_subnets",
  "Netboot::NetworkSchema::Result::Ip6Subnets",
  { "foreign.network" => "self.network" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QPemPl2EP0R/kzZAAX319w


# You can replace this text with custom content, and it will be preserved on regeneration
1;
