package Netboot::Controller::Manage;
use Moose;
use namespace::autoclean;

use File::Copy qw/cp/;
use File::Copy::Recursive qw/fcopy dircopy/;
use File::Path;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Netboot::Controller::manage - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->redirect($c->uri_for($self->action_for('computers')));
}

sub convert_ip :Private {
    my ( $self, $ip ) = @_;
    my $hex_ip = "";

    while ($ip =~ /(\d+)/g) {
        $hex_ip = $hex_ip . sprintf("%02x", $1);
    }

    return $hex_ip;
}

sub get_option_name :Private {
    my ( $self, $option ) = @_;

    $option =~ s/_/ /g;
    $option =~ /(.+)=.+/;

    return (uc $1);
}

sub get_option_value :Private {
    my ( $self, $option ) = @_;

    $option =~ /.+=(\.+)/;
    return $1;
}

sub options :Local {
    my ( $self, $c ) = @_;

    my $hostname = $c->request->parameters->{hostname};
    my $ipaddr = $c->request->parameters->{ip};
    my $image_name = $c->request->parameters->{image_name};

    $c->stash(user_images => [$c->model('DB::Images')->user_images('worr')->image_information]);
    $c->stash(options => [$c->model('DB::Images')->search({ 'me.name' => $image_name })->first->options->[0]]);
    $c->stash(hostname => $hostname);
    $c->stash(ipaddr => $ipaddr);
    $c->stash(image_name => $image_name);
    $c->stash(template => 'manage/options.mas');
}

sub computers :Local {
    my ( $self, $c ) = @_;

    $c->stash(types => [$c->model('DB::Types')->type_names]);
    $c->stash(user_images => [$c->model('DB::Images')->user_images('worr')->image_information]);
    $c->stash(images => [$c->model('DB::Images')->image_information]);
    $c->stash(template => 'manage/computers.mas');
}

sub make_pxelinux :Private {
    my ( $self, $c, $ipaddr, $hostname, $image_name, $options ) = @_;

    $c->model('NetworkDB::ClassOptions')->create({
                                                    class => "nb_$hostname",
                                                    option => "next-server",
                                                    value => "129.21.50.7",
                                                });

    $c->model('NetworkDB::ClassOptions')->create({
                                                    class => "nb_$hostname",
                                                    option => "filename",
                                                    value => '"/srv/tftpboot/pxelinux.0"',
                                                });

    $c->model('NetworkDB::ClassOptions')->create({
                                                    class => "nb_$hostname",
                                                    option => "match pick-first-value",
                                                    value => "(option dhcp-client-identifier, hardware)",
                                                });

    my $pxelinux = $c->model('DB::Images')->search({ name => "$image_name" })->first->pxelinux_id->pxelinux_cfg;
    my $host_cfg_base = $self->convert_ip($ipaddr);
    my $host_cfg = "/srv/tftpboot/pxelinux.cfg/$host_cfg_base";

    cp("/srv/images/pxelinux/$image_name/pxelinux.cfg", "$host_cfg") or die "$?";

    open(PXELINUX, "$pxelinux");
    open(HOST_CFG, ">$host_cfg");

    my ($option_name, $option_value);
    foreach my $line (<PXELINUX>) {
        map { $option_name = $self->get_option_name($_);
              $option_value = $self->get_option_value($_);
              $line =~ s/$option_name/$option_value/g } @$options;
        print HOST_CFG $line;
    }

    print HOST_CFG $pxelinux;
    close PXELINUX;
    close HOST_CFG;

    dircopy("/srv/images/pxelinux/$image_name/tftproot", "/srv/tftpboot/$image_name") or die "$?";
}

sub boot_computer :Local {
    my ( $self, $c ) = @_;

    my $hostname = $c->request->parameters->{hostname};
    my $ipaddr = $c->request->parameters->{ipaddr};
    my $image_name = $c->request->parameters->{image_name};

    delete($c->request->parameters->{hostname});
    delete($c->request->parameters->{ipaddr});
    delete($c->request->parameters->{image_name});
    my $options = $c->request->parameters;

    my @db_options = map { "$_=$options->{$_}" } keys %$options;
    my $user_id = $c->model('DB::Users')->search({ username => "worr" })->first->id;

    $c->model('DB::Hosts')->create({
                                        hostname => $hostname,
                                        ipaddr => $ipaddr,
                                        user_id => $user_id,
                                        options => \@db_options,
                                    });

    $c->model('NetworkDB::Classes')->create({
                                        name => "nb_$hostname",
                                    });

    $c->model('NetworkDB::Hosts')->search({ 
                                        hostname => "$hostname",
                                        username => "worr",
                                    })->update({
                                        class => "nb_$hostname",
                                    });

    if ($c->model('DB::Images')->search({ name => "$image_name" })->first->type_id->name eq "pxelinux") {
        $self->make_pxelinux($c, $ipaddr, $hostname, $image_name, \@db_options);
    }

    $c->stash(user_images => [$c->model('DB::Images')->user_images('worr')->image_information]);
    $c->stash(template => 'manage/boot_completed.mas');
}

sub images :Local {
    my ( $self, $c ) = @_;

    $c->stash(user_images => [$c->model('DB::Images')->user_images('worr')->image_information]);
    $c->stash(images => [$c->model('DB::Images')->image_information]);
    $c->stash(is_admin => [$c->model('DB::Users')->isadmin('worr')]);
    $c->stash(template => 'manage/images.mas');
}

sub image_delete :Local {
    my ( $self, $c ) = @_;
    my $deleted = [ $c->req->parameters->{delete} ];

    foreach my $image (@$deleted) {
        my $db_image = $c->model('DB::Images')->search({ "me.id" => "$image" });
        my $db_image_info = $db_image->image_information;
        my $type_name = $db_image_info->first->type_id->name;
        my $image_name = $db_image->first->name;
        my $type_spec_id;
        my $db_image_type_spec;

        if ($type_name eq "pxelinux") {
            $type_spec_id = $db_image->first->pxelinux_id;
            $db_image_type_spec = $c->model('DB::Pxelinux')->search({ "me.id" => "$type_spec_id" });
        } elsif ($type_name eq "pxefreebsd") {
            $type_spec_id = $db_image->first->pxefreebsd_id;
            $db_image_type_spec = $c->model('DB::Pxefreebsd')->search({ "me.id" => "$type_spec_id" });
        } elsif ($type_name eq "pxegrub") {
            $type_spec_id = $db_image->first->pxegrub_id;
            $db_image_type_spec = $c->model('DB::Pxegrub')->search({ "me.id" => "$type_spec_id" });
        }

        $db_image->delete;
        $type_spec_id->delete;

        chdir("/srv/images");
        rmtree("/srv/images/$type_name/$image_name");
    }

    $c->stash(user_images => [$c->model('DB::Images')->user_images('worr')->image_information]);
    $c->stash(template => 'manage/delete_completed.mas');
}


=head1 AUTHOR

,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
