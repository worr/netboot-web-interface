package Netboot::NetworkSchema::Result::Hosts;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("hosts");
__PACKAGE__->add_columns(
  "hardware_address",
  { data_type => "macaddr", default_value => undef, is_nullable => 0, size => 6 },
  "ip_address",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "hostname",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "username",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "auth_user",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "date_entered",
  {
    data_type => "timestamp without time zone",
    default_value => "now()",
    is_nullable => 0,
    size => 8,
  },
  "last_seen",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 1,
    size => 8,
  },
  "wireless",
  {
    data_type => "boolean",
    default_value => "false",
    is_nullable => 0,
    size => 1,
  },
  "static",
  {
    data_type => "boolean",
    default_value => "false",
    is_nullable => 0,
    size => 1,
  },
  "comments",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "domain",
  {
    data_type => "text",
    default_value => "'csh.rit.edu'::text",
    is_nullable => 0,
    size => undef,
  },
  "class",
  {
    data_type => "text",
    default_value => "'default'::text",
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("hardware_address");
__PACKAGE__->add_unique_constraint("hosts_ip_address_key", ["ip_address"]);
__PACKAGE__->add_unique_constraint("hosts_hostname_key", ["hostname"]);
__PACKAGE__->add_unique_constraint("hosts_pkey", ["hardware_address"]);
__PACKAGE__->has_many(
  "dhcp_options",
  "Netboot::NetworkSchema::Result::DhcpOptions",
  { "foreign.hardware_address" => "self.hardware_address" },
);
__PACKAGE__->belongs_to(
  "ip_address",
  "Netboot::NetworkSchema::Result::HostCache",
  { ip_address => "ip_address" },
);
__PACKAGE__->belongs_to(
  "domain",
  "Netboot::NetworkSchema::Result::DnsZones",
  { zone => "domain" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:mLqX4eE2dAvYuPwDMWtSww


# You can replace this text with custom content, and it will be preserved on regeneration
1;
