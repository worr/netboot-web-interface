package Netboot::NetworkSchema::Result::FwMhMembers;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("fw_mh_members");
__PACKAGE__->add_columns(
  "ip_addr",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "metahost",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8sjElhC5dw3ymU3PkAyRIQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
