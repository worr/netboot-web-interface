package Netboot::NetworkSchema::Result::DnsQueueDetail;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("dns_queue_detail");
__PACKAGE__->add_columns(
  "dns_state",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "dns_update",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "dns_zone",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "key_name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "key",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vX0b52fSffBErR5iNALatA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
