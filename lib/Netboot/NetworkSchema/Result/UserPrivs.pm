package Netboot::NetworkSchema::Result::UserPrivs;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("user_privs");
__PACKAGE__->add_columns(
  "priv",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 16,
  },
  "username",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("priv", "username");
__PACKAGE__->add_unique_constraint("user_privs_pkey", ["priv", "username"]);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fAYvPsiomCP4kSm9xyJDKQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
