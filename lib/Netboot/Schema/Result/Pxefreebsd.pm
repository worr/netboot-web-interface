package Netboot::Schema::Result::Pxefreebsd;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("pxefreebsd");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('pxefreebsd_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "pxeboot",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "loader_rc",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "tftproot",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "nfsroot",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("pxefreebsd_pxeboot_key", ["pxeboot"]);
__PACKAGE__->add_unique_constraint("pxefreebsd_pkey", ["id"]);
__PACKAGE__->add_unique_constraint("pxefreebsd_tftproot_key", ["tftproot"]);
__PACKAGE__->add_unique_constraint("pxefreebsd_nfsroot_key", ["nfsroot"]);
__PACKAGE__->add_unique_constraint("pxefreebsd_loader_rc_key", ["loader_rc"]);
__PACKAGE__->has_many(
  "images",
  "Netboot::Schema::Result::Images",
  { "foreign.pxefreebsd_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PYwE5oeOOBfn04Y4jPXUZg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
