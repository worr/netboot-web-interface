package Netboot::NetworkSchema::Result::SubnetOptions;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("subnet_options");
__PACKAGE__->add_columns(
  "subnet",
  {
    data_type => "cidr",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "option",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "value",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("subnet", "option");
__PACKAGE__->add_unique_constraint("subnet_options_pkey", ["subnet", "option"]);
__PACKAGE__->belongs_to(
  "subnet",
  "Netboot::NetworkSchema::Result::Subnets",
  { subnet => "subnet" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dTK2TsVbWhpSwYKGM/d4Ig


# You can replace this text with custom content, and it will be preserved on regeneration
1;
