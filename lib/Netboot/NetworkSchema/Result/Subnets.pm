package Netboot::NetworkSchema::Result::Subnets;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("subnets");
__PACKAGE__->add_columns(
  "subnet",
  {
    data_type => "cidr",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "network",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("subnet");
__PACKAGE__->add_unique_constraint("subnets_pkey", ["subnet"]);
__PACKAGE__->has_many(
  "address_ranges",
  "Netboot::NetworkSchema::Result::AddressRanges",
  { "foreign.subnet" => "self.subnet" },
);
__PACKAGE__->has_many(
  "subnet_options",
  "Netboot::NetworkSchema::Result::SubnetOptions",
  { "foreign.subnet" => "self.subnet" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4FKnp4bg22xKKZDmS9bfVA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
