package Netboot::NetworkSchema::Result::DnsQueue;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("dns_queue");
__PACKAGE__->add_columns(
  "dns_zone",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "dns_update",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "dns_time",
  {
    data_type => "timestamp without time zone",
    default_value => "now()",
    is_nullable => 0,
    size => 8,
  },
  "dns_state",
  { data_type => "integer", default_value => 0, is_nullable => 0, size => 4 },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VxgSwQCfLaYgZaihQGfynQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
