use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Netboot' }
BEGIN { use_ok 'Netboot::Controller::List' }

ok( request('/list')->is_success, 'Request should succeed' );
done_testing();
