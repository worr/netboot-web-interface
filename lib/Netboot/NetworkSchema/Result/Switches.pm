package Netboot::NetworkSchema::Result::Switches;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("switches");
__PACKAGE__->add_columns(
  "hostname",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "ip_address",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "hardware_address",
  { data_type => "macaddr", default_value => undef, is_nullable => 1, size => 6 },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "location",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("ip_address");
__PACKAGE__->add_unique_constraint("switches_hardware_address_key", ["hardware_address"]);
__PACKAGE__->add_unique_constraint("switches_pkey", ["ip_address"]);
__PACKAGE__->add_unique_constraint("switches_hostname_key", ["hostname"]);
__PACKAGE__->has_many(
  "ports",
  "Netboot::NetworkSchema::Result::Ports",
  { "foreign.switch" => "self.ip_address" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kzfgMCQJ1YcCLNIDzoYrrA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
