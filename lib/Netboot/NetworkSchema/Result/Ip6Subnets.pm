package Netboot::NetworkSchema::Result::Ip6Subnets;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("ip6_subnets");
__PACKAGE__->add_columns(
  "prefix",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "network",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("prefix");
__PACKAGE__->add_unique_constraint("ip6_subnets_pkey", ["prefix"]);
__PACKAGE__->belongs_to(
  "network",
  "Netboot::NetworkSchema::Result::Networks",
  { network => "network" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yu797SHlMTmTImMVyavHxg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
