package Netboot::NetworkSchema::Result::ProcessQueue;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("process_queue");
__PACKAGE__->add_columns(
  "event_class",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 10,
  },
  "event_state",
  { data_type => "integer", default_value => 0, is_nullable => 0, size => 4 },
  "event_time",
  {
    data_type => "timestamp without time zone",
    default_value => "now()",
    is_nullable => 0,
    size => 8,
  },
  "event_data",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:431x/Dm0+xWg38YGYv/luQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
