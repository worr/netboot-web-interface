package Netboot::View::Mason;

use strict;
use warnings;

use parent 'Catalyst::View::Mason';

__PACKAGE__->config(use_match => 0,
                    comp_root => Netboot->path_to('root', 'src'),);

=head1 NAME

Netboot::View::Mason - Mason View Component for Netboot

=head1 DESCRIPTION

Mason View Component for Netboot

=head1 SEE ALSO

L<Netboot>, L<HTML::Mason>

=head1 AUTHOR

,,,

=head1 LICENSE

This library is free software . You can redistribute it and/or modify it under
the same terms as perl itself.

=cut

1;
