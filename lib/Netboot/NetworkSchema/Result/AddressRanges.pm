package Netboot::NetworkSchema::Result::AddressRanges;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("address_ranges");
__PACKAGE__->add_columns(
  "first_ip",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "last_ip",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "subnet",
  {
    data_type => "cidr",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "use",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "domain",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("first_ip");
__PACKAGE__->add_unique_constraint("address_ranges_last_ip_key", ["last_ip"]);
__PACKAGE__->add_unique_constraint("address_ranges_pkey", ["first_ip"]);
__PACKAGE__->belongs_to(
  "subnet",
  "Netboot::NetworkSchema::Result::Subnets",
  { subnet => "subnet" },
);
__PACKAGE__->belongs_to(
  "domain",
  "Netboot::NetworkSchema::Result::DnsZones",
  { zone => "domain" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FqvGSar6AZjCUoUUxTRegA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
