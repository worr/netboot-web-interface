package Netboot::Schema::Result::Types;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("types");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('types_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "name",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 15,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("types_pkey", ["id"]);
__PACKAGE__->has_many(
  "images",
  "Netboot::Schema::Result::Images",
  { "foreign.type_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7JVb7OzukqD+rh+at8Z8ng


# You can replace this text with custom content, and it will be preserved on regeneration
1;
