package Netboot::NetworkSchema::Result::ClassOptions;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("class_options");
__PACKAGE__->add_columns(
  "class",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "option",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "value",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("class", "option");
__PACKAGE__->add_unique_constraint("class_options_pkey", ["class", "option"]);
__PACKAGE__->belongs_to(
  "class",
  "Netboot::NetworkSchema::Result::Classes",
  { name => "class" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:r16lfNvjJdNHdL2hQ6Gw0w


# You can replace this text with custom content, and it will be preserved on regeneration
1;
