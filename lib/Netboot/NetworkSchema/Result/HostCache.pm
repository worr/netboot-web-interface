package Netboot::NetworkSchema::Result::HostCache;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("host_cache");
__PACKAGE__->add_columns(
  "ip_address",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "in_use",
  {
    data_type => "boolean",
    default_value => "false",
    is_nullable => 0,
    size => 1,
  },
);
__PACKAGE__->set_primary_key("ip_address");
__PACKAGE__->add_unique_constraint("host_cache_pkey", ["ip_address"]);
__PACKAGE__->has_many(
  "hosts",
  "Netboot::NetworkSchema::Result::Hosts",
  { "foreign.ip_address" => "self.ip_address" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8ZBJNWXFVNjM2AKA9V49EQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
