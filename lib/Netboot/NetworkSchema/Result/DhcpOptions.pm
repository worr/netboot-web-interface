package Netboot::NetworkSchema::Result::DhcpOptions;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("dhcp_options");
__PACKAGE__->add_columns(
  "hardware_address",
  { data_type => "macaddr", default_value => undef, is_nullable => 0, size => 6 },
  "option_name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "option_value",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("hardware_address", "option_name");
__PACKAGE__->add_unique_constraint("dhcp_options_pkey", ["hardware_address", "option_name"]);
__PACKAGE__->belongs_to(
  "hardware_address",
  "Netboot::NetworkSchema::Result::Hosts",
  { hardware_address => "hardware_address" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cS9abadM+JM9Y0M9nNxHDQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
