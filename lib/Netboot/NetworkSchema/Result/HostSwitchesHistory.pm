package Netboot::NetworkSchema::Result::HostSwitchesHistory;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("host_switches_history");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('host_switches_history_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "hardware_address",
  { data_type => "macaddr", default_value => undef, is_nullable => 0, size => 6 },
  "switch",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "port",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "start_time",
  {
    data_type => "timestamp without time zone",
    default_value => "now()",
    is_nullable => 0,
    size => 8,
  },
  "end_time",
  {
    data_type => "timestamp without time zone",
    default_value => "now()",
    is_nullable => 0,
    size => 8,
  },
  "active",
  { data_type => "integer", default_value => 1, is_nullable => 0, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("host_switches_history_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "port",
  "Netboot::NetworkSchema::Result::Ports",
  { port => "port", switch => "switch" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yfmRSoW2iPNI2MsOy6fQQg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
