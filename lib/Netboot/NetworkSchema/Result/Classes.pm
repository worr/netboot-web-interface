package Netboot::NetworkSchema::Result::Classes;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("classes");
__PACKAGE__->add_columns(
  "name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("name");
__PACKAGE__->add_unique_constraint("classes_pkey", ["name"]);
__PACKAGE__->has_many(
  "class_options",
  "Netboot::NetworkSchema::Result::ClassOptions",
  { "foreign.class" => "self.name" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kF16+2tMsxw3PCHAYijzLQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
