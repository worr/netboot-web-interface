package Netboot::Schema::ResultSet::Types;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub type_names {
    my ($self) = @_;

    return $self->search(undef, { 'columns' => 'name' });
}

sub get_id {
    my ( $self, $type ) = @_;
    
    return $self->search({ 'name' => "$type" }, { 'select' => 'id' });
}

1;
