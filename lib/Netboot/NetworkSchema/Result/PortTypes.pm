package Netboot::NetworkSchema::Result::PortTypes;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("port_types");
__PACKAGE__->add_columns(
  "port_type",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("port_type");
__PACKAGE__->add_unique_constraint("port_types_pkey", ["port_type"]);
__PACKAGE__->has_many(
  "ports",
  "Netboot::NetworkSchema::Result::Ports",
  { "foreign.port_type" => "self.port_type" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gxtCc1V9GM38WM3j8GoAyw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
