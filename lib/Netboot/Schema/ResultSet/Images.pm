package Netboot::Schema::ResultSet::Images;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

sub image_information {
    my ($self) = @_;

    return $self->search(undef, { join => ['type_id', 'user_id'],
                                  '+select' => 'type_id.name',
                                  '+select' => 'user_id.username'});
}

sub pxefreebsd {
    my ($self) = @_;

    return $self->search({'type_id.name' => 'pxefreebsd'}, { join => 'pxefreebsd_id',
                                  '+select' => ['pxefreebsd_id.pxeboot',
                                                'pxefreebsd_id.loader_rc',
                                                'pxefreebsd_id.tftproot',
                                                'pxefreebsd_id.nfsroot']});
}

sub pxelinux {
    my ($self) = @_;

    return $self->search({'type_id.name' => 'pxelinux'}, { join => 'pxelinux_id',
                                  '+select' => ['pxelinux_id.pxelinux_cfg',
                                                'pxelinux_id.tftproot']});
}

sub pxegrub {
    my ($self) = @_;

    return $self->search({'type_id.name' => 'pxegrub'}, { join => 'pxegrub_id',
                                  '+select' => ['pxegrub_id.pxelinux_cfg',
                                                'pxegrub_id.grub_lst',
                                                'pxegrub_id.kernel',
                                                'pxegrub_id.initrd',
                                                'pxegrub_id.nfsroot']});
}

sub user_images {
    my ($self, $user) = @_;

    return $self->search({'user_id.username' => "$user"}, { join => 'user_id',
                                                           '+select' => 'user_id.username'});
}

1;
