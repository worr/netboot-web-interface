package Netboot::Schema::Result::Pxegrub;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("pxegrub");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('pxegrub_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "pxelinux_cfg",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "grub_lst",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "kernel",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
  "initrd",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 75,
  },
  "nfsroot",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 0,
    size => 75,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("pxegrub_pkey", ["id"]);
__PACKAGE__->add_unique_constraint("pxegrub_pxelinux_cfg_key", ["pxelinux_cfg"]);
__PACKAGE__->add_unique_constraint("pxegrub_kernel_key", ["kernel"]);
__PACKAGE__->add_unique_constraint("pxegrub_initrd_key", ["initrd"]);
__PACKAGE__->add_unique_constraint("pxegrub_grub_lst_key", ["grub_lst"]);
__PACKAGE__->add_unique_constraint("pxegrub_nfsroot_key", ["nfsroot"]);
__PACKAGE__->has_many(
  "images",
  "Netboot::Schema::Result::Images",
  { "foreign.pxegrub_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-03-07 18:11:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YopfDZovOi9y9ask49sndQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
