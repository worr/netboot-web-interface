package Netboot::NetworkSchema::Result::FwLastUpdate;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("fw_last_update");
__PACKAGE__->add_columns(
  "last_update",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 1,
    size => 8,
  },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:IoXIduqNVRtYm9koSXiLKQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
