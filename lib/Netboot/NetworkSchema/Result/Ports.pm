package Netboot::NetworkSchema::Result::Ports;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("ports");
__PACKAGE__->add_columns(
  "switch",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "port",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "port_type",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "room",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "jack",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "comment",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "active",
  { data_type => "boolean", default_value => undef, is_nullable => 0, size => 1 },
  "change_pending",
  { data_type => "boolean", default_value => undef, is_nullable => 0, size => 1 },
  "last_change",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 0,
    size => 8,
  },
  "last_change_user",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "last_change_reason",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("switch", "port");
__PACKAGE__->add_unique_constraint("ports_pkey", ["switch", "port"]);
__PACKAGE__->has_many(
  "host_switches_histories",
  "Netboot::NetworkSchema::Result::HostSwitchesHistory",
  { "foreign.port" => "self.port", "foreign.switch" => "self.switch" },
);
__PACKAGE__->has_many(
  "port_changes",
  "Netboot::NetworkSchema::Result::PortChanges",
  { "foreign.port" => "self.port", "foreign.switch" => "self.switch" },
);
__PACKAGE__->belongs_to(
  "switch",
  "Netboot::NetworkSchema::Result::Switches",
  { ip_address => "switch" },
);
__PACKAGE__->belongs_to(
  "port_type",
  "Netboot::NetworkSchema::Result::PortTypes",
  { port_type => "port_type" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AKd8a6m6Ocpivp9sXJndSw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
