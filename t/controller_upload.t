use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Netboot' }
BEGIN { use_ok 'Netboot::Controller::upload' }

ok( request('/upload')->is_success, 'Request should succeed' );
done_testing();
