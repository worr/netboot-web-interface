package Netboot::NetworkSchema::Result::FwBlockAll;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");
__PACKAGE__->table("fw_block_all");
__PACKAGE__->add_columns(
  "enable",
  {
    data_type => "boolean",
    default_value => "true",
    is_nullable => 0,
    size => 1,
  },
  "id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "type",
  {
    data_type => "character",
    default_value => undef,
    is_nullable => 0,
    size => 1,
  },
  "ip_addr",
  {
    data_type => "inet",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "dir",
  {
    data_type => "character",
    default_value => undef,
    is_nullable => 0,
    size => 1,
  },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2010-05-08 08:29:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oAuMSwkYVRP8qUmTP8yKfQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
